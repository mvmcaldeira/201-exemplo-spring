package tech.mastertech.itau.produto.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import tech.mastertech.itau.produto.models.Fornecedor;
import tech.mastertech.itau.produto.repositories.FornecedorRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = FornecedorService.class)
public class FornecedorServiceTest {
  @Autowired
  private FornecedorService sujeito;
  
  @MockBean
  private FornecedorRepository fornecedorRepository;
  
  @Test
  public void deveCadastrarUmFornecedor() {
    String senha = "admin123";
    
    Fornecedor fornecedor = new Fornecedor();
    fornecedor.setId(1);
    fornecedor.setNome("Maneu");
    fornecedor.setNomeUsuario("maneuuser");
    fornecedor.setSenha(senha);
    
    when(fornecedorRepository.save(fornecedor)).thenReturn(fornecedor);
    
    Fornecedor fornecedorSalvo = sujeito.setFornecedor(fornecedor);
    
    verify(fornecedorRepository).save(fornecedor);
    
    assertEquals(fornecedor.getId(), fornecedorSalvo.getId());
    assertEquals(fornecedor.getNome(), fornecedorSalvo.getNome());
    assertEquals(fornecedor.getNomeUsuario(), fornecedorSalvo.getNomeUsuario());
    assertNotEquals(senha, fornecedorSalvo.getSenha());
  }
}
