package tech.mastertech.itau.produto.repositories;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.produto.models.Pedido;

public interface PedidoRepository extends CrudRepository<Pedido, Integer> {
}
